## Common.mk
## by: Cameron Wong

# environment
INSTALL_DIR = /opt/microc
BUILD_DIR = $(TOP)/build
CPPFLAGS =
LDFLAGS =
LIBS =

include $(TOP)/Mode.mk

# configuration
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))

# build flags
COMMON_CPP = -fPIC -std=gnu++17
CPPFLAGS += $(COMMON_CPP) -MMD -MP -Wall -Werror -Wextra
CPPFLAGS += -I..
CPPFLAGS += -I$(TOP)/include
CPPFLAGS += -I$(TOP)/src
LDFLAGS += $(COMMON_CPP)
LDFLAGS += -Wl,-z,defs $(LIBS)

include $(TOP)/Dependency.mk

# customs
.PHONY: all clean cleanall distclean check install uninstall
.SECONDARY:
.SECONDEXPANSION:

# rules
first: all
