## Mode.mk
## by: Cameron Wong

ifneq (,$(findstring release,$(BUILD)))
        buildFLAGS +=-g1 -O3
	linkFLAGS +=-Wl,-O1 -Wl,-s
else ifneq (,$(findstring debug,$(BUILD)))
        buildFLAGS +=-g3 -ggdb3 -O0 -DMICROC_DEBUG
	linkFLAGS +=-O0
else ifneq (,$(findstring test,$(BUILD)))
        buildFLAGS +=-g1 -O3
	linkFLAGS +=-Wl,-O1
	INSTALL_DIR = /opt/microc
else
	# debug build
        buildFLAGS +=-g3 -ggdb3 -O0 -DMICROC_DEBUG
	linkFLAGS +=-O0
endif

CPPFLAGS += $(buildFLAGS)
LDFLAGS += $(linkFLAGS)
