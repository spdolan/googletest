## cpp-log
## by: Cameron Wong

# locative
TOP	= $(shell git rev-parse --show-toplevel)

# environment
SUBDIRS = src
TARGETS = all install clean cleanall

first: all

$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

$(TARGETS): $(SUBDIRS)

.PHONY: all install clean cleanall $(SUBDIRS)
.SECONDARY:
